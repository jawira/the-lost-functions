`\Jawira\TheLostFunctions\year_range`
=======================================

**year_range** - Creates a string containing a range of years.

Description
-----------

```php
year_range(int $startYear): string
```

Will return a range if `$startYear` is greater than current year, otherwise only
current year is returned.

This function is useful for website's footers, this is where year ranges are
commonly displayed.

Parameters
----------

<dl>
<dt>startYear</dt>
<dd>Initial year from the range.</dd>
</dl>

Return value
------------

A string containing a range.
[En dash](https://en.wikipedia.org/wiki/Dash#En_dash) is used.

Example
-------

Following examples were executed in _2021_:

```php
use function Jawira\TheLostFunctions\year_range;

year_range(2013); // 2013–2021
year_range(2020); // 2020–2021
year_range(2021); // 2021
year_range(2025); // 2025
```
