`\Jawira\TheLostFunctions\is_initialized`
====================================

**is_initialized** - Tells you if an object's property has been initialized.

This function is very handy when working with typed properties.

Description
-----------

```php
is_initialized(object $object, string $property): bool
```

`is_initialized` will work properly with _public_, _protected_, or _private_
properties.

An `\ReflectionException` will be thrown if you provide an invalid property
name.

Parameters
----------

<dl>
<dt>object</dt>
<dd>Object we want to check.</dd>
<dt>property</dt>
<dd>The name of the property to check.</dd>
</dl>

Return value
------------

`true` if property has been initialized, `false` otherwise.

Example
-------

Because `name` is a typed property and has no value, when you try to get the
value of `name` you get a _fatal error_:

```php
echo $student->name;
```

```
PHP Fatal error:  Uncaught Error: Typed property Student::$name must not be accessed before initialization
```

Use `is_initialized` function to check if a property has been initialized.

```php
if (is_initialized($student, 'name')) {
  echo $student->name;
}
```

Full example:

```php
use function Jawira\TheLostFunctions\is_initialized;

class Student
{
  public string $name;
}

$student = new Student();

if (is_initialized($student, 'name')) {
  echo $student->name;
}
```
