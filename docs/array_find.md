`\Jawira\TheLostFunctions\array_find`
=====================================

**array_find** - Searches the array for a given value and returns the
**first element** that `$callback` return `true`.

Description
-----------

```php
array_find(iterable $array, ?callable $callback = null): mixed
```

Searches an element using a callback.

Parameters
----------

<dl>
<dt>array</dt>
<dd>The array or an object implementing <code>\Traversable interface</code>.</dd>
<dt>callback</dt>
<dd>The callback function to use. If no callback is supplied, the first non
empty value will be returned.</dd>
</dl>

Return value
------------

Returns the element if it's found in the array, `null` otherwise.

Example
-------

```php
use function Jawira\TheLostFunctions\array_search_callback;

$demo = [null, false, '', 'foo', 'bar', '321', 1000];
array_find($demo, fn($item) => is_int($item)); // 1000
array_find($demo, 'is_numeric'); // 321
array_find($demo); // foo
```
