`\Jawira\TheLostFunctions\throw_unless`
=======================================

**throw_unless** - Throws an _exception_ unless provided expression evaluates to
_true_.

Description
-----------

```php
throw_unless(mixed $expression, ?Throwable $throwable = null): mixed
```

This function can replace `assert()` function. Unlike `assert()`,
`throw_unless()` cannot be disabled.

Parameters
----------

<dl>
<dt>expression</dt>
<dd>Any valid PHP expression.</dd>
<dt>throwable</dt>
<dd>An optional throwable object. If not provided <code>\Exception</code> will be used.</dd>
</dl>

Return value
------------

If you passed a value as parameter, then the same value will be returned. If you used an expression _$expression_, the result of the expression will be returned.

Example
-------

I wanted to use `assert()` this way:

```php
assert($user instanceof \App\Entity\User, new Exception('Invalid user'));
```

Because assert can be disabled, I had to rewrite the code:

```php
if(!($user instanceof \App\Entity\User)) {
    throw new Exception('Invalid user');
}
```

Using `throw_unless()` I can write this in a single line again:

```php
use function Jawira\TheLostFunctions\throw_unless;

throw_unless($user instanceof \App\Entity\User, new Exception('Invalid user'));
```
