`\Jawira\TheLostFunctions\temp_file`
====================================

**temp_file** - Creates a temporary file.

Description
-----------

```php
temp_file(string $prefix): \SplFileObject
```

Will create a temporary file to work with.

Temporary directory can be configured using `sys_temp_dir` ini config.
An exception is thrown if temporary file cannot be created.

Tip: Consider using `clearstatcache()` if you are working with files.

Parameters
----------

<dl>
<dt>prefix</dt>
<dd>Prefix used in filename, empty string can be used.</dd>
</dl>

Return value
------------

Returns temporary file as `\SplFileObject` object.

Example
-------

```php
use function Jawira\TheLostFunctions\temp_file;

$tempFile = temp_file('foo');
$tempFile->fwrite('Hello world.' . PHP_EOL);
echo $tempFile->getPathname(); // /tmp/fooqPfAXs
```
