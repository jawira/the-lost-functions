`\Jawira\TheLostFunctions\get_short_class`
=======================================

**get_short_class** - Returns the short name of the class of an object.

Description
-----------

```php
get_short_class(string|object $object): string
```

Gets the short name of the class of the given object.

Parameters
----------

<dl>
<dt>object</dt>
<dd>An object or a string of the object's full namespace.</dd>
</dl>

Return value
------------

The object's short name.

Example
-------

```php
use function Jawira\TheLostFunctions\get_short_class;

$demo = new \Foo\Bar\Baz();
get_short_class($demo); // Baz
get_short_class(\App\Demo\User::class); // User
```
