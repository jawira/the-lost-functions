`\Jawira\TheLostFunctions\str_bytes`
====================================

**str_bytes** - Get string length in bytes.

Description
-----------

PHP has a 'function overloading' feature, it can be enabled
using [`mbstring.func_overload`](https://www.php.net/manual/en/mbstring.overload.php)
option in `php.ini`. This option changes the behaviour of `strlen`, therefore
this function is not reliable anymore to measure the size of a string in bytes.

Use `str_bytes` to get the size of a string in bytes. This function is not
affected by 'function overloading'.

```php
str_bytes(string $string): int
```

Parameters
----------

<dl>
<dt>string</dt>
<dd>The string being measured for length.</dd>
</dl>

Return value
------------

The size of the `string` in bytes.

Example
-------

```php
use function Jawira\TheLostFunctions\str_bytes;

str_bytes('hello'); // 5
str_bytes('Único'); // 6
```
