`\Jawira\TheLostFunctions\year`
===============================

**year** - Returns current year.

Description
-----------

```php
year(bool $short = false, ?DateTimeInterface $dateTime = null): string
```

Will return current _year_ as a `string`. When the first parameter `$short` is
set to `true` two digit year is returned.

You can also pass a `DateTime` object as a second argument, the function will
return the year of the given object.

Parameters
----------

<dl>
<dt>short</dt>
<dd>Return a two digits year.</dd>
<dt>dateTime</dt>
<dd>A `DateTime` object.</dd>
</dl>

Return value
------------

A year as `string`.

Example
-------

Following examples were executed in _2021_:

```php
use function Jawira\TheLostFunctions\year;

year();     // 2021
year(true); // 21
year(false, new DateTime('2015-01-12')); // 2015
```
