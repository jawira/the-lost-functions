The Lost Functions
==================

**This is a collection of convenience functions.**

**I use these functions in my projects, I add new functions according to my needs.**

How to install
--------------

```console
composer require jawira/the-lost-functions
```

Contributing
------------

If you liked this project,
⭐ [star it on GitLab](https://gitlab.com/jawira/the-lost-functions).
