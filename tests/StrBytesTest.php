<?php

namespace Jawira\TheLostFunctions\Tests;

use PHPUnit\Framework\TestCase;
use function Jawira\TheLostFunctions\str_bytes;

class StrBytesTest extends TestCase
{
  /**
   * @dataProvider strBytesProvider
   * @covers       \Jawira\TheLostFunctions\str_bytes
   */
  public function testStrBytes($string = 'hello', $expected = 5)
  {
    $result = str_bytes($string);
    $this->assertSame($expected, $result);
  }

  public function strBytesProvider()
  {
    return [
      ['', 0],
      ['hello', 5],
      ['🐈‍⬛', 10],
      ['é', 2],
      ['🤦🏾‍♂️', 17],
      ['ààààà', 10],
      ['Único', 6],
    ];
  }
}
