<?php

namespace Jawira\TheLostFunctions\Tests;

use PHPUnit\Framework\TestCase;
use function Jawira\TheLostFunctions\year_range;
use function strval;

class YearRangeTest extends TestCase
{
  /**
   * @covers       \Jawira\TheLostFunctions\year_range
   * @dataProvider dateRangeProvider
   */
  public function testDateRange($startYear = 2019, $expected = '2019–2021')
  {
    $result = year_range($startYear);
    $this->assertSame($expected, $result);
  }

  public function dateRangeProvider()
  {
    $now = date('Y');

    return [
      [$now, strval($now)],
      [2010, "2010\u{2013}$now"],
      [6000, strval($now)],
    ];
  }
}
