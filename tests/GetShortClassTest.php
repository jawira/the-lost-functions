<?php declare(strict_types=1);


namespace Jawira\TheLostFunctions\Tests;


use PHPUnit\Framework\TestCase;
use stdClass;
use TypeError;
use function Jawira\TheLostFunctions\get_short_class;

class GetShortClassTest extends TestCase
{
  /**
   * @covers       \Jawira\TheLostFunctions\get_short_class
   * @dataProvider getShortNameProvider
   */
  public function testGetShortName($className = '\\DateTime', $expected = 'DateTime')
  {
    $actual = get_short_class($className);

    $this->assertSame($expected, $actual);
  }

  public function getShortNameProvider()
  {
    $myObject = new stdClass();

    return [
      ['\\DateTime', 'DateTime'],
      [\This\Is\Valid::class, 'Valid'],
      [\DateTime::class, 'DateTime'],
      [Demo::class, 'Demo'],
      ['Abc\\Xyz', 'Xyz'],
      ['Abc\\', ''],
      ['', ''],
      ['Hello', 'Hello'],
      ['Moño\\Güero', 'Güero'],
      [$myObject, 'stdClass'],
      [$this, 'GetShortClassTest'],
    ];
  }

  /**
   * @covers       \Jawira\TheLostFunctions\get_short_class
   * @dataProvider cannotGetShortNameProvider
   */
  public function testCannotGetShortName($className)
  {
    $this->expectException(TypeError::class);
    get_short_class($className);
  }

  public function cannotGetShortNameProvider()
  {
    return [
      [456123],
      [true],
    ];
  }
}
