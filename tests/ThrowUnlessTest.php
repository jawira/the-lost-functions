<?php

namespace Jawira\TheLostFunctions\Tests;

use PHPUnit\Framework\TestCase;
use stdClass;
use function Jawira\TheLostFunctions\throw_unless;

class ThrowUnlessTest extends TestCase
{
  /**
   * @covers       \Jawira\TheLostFunctions\throw_unless
   * @dataProvider normalCaseProvider
   */
  public function testNormalCase($expression = 'hello', $expected = 'hello')
  {
    $result = throw_unless($expression);

    $this->assertSame($expected, $result);
  }

  public function normalCaseProvider()
  {
    $dummy = new stdClass();

    return [[5 + 9, 14],
      ['hello', 'hello'],
      [true, true],
      ['1', '1'],
      [$dummy instanceof stdClass, true],];
  }

  /**
   * @covers       \Jawira\TheLostFunctions\throw_unless
   * @dataProvider throwExceptionProvider
   */
  public function testThrowException($expression, $exception, $exceptionClass, $exceptionMessage)
  {
    $this->expectException($exceptionClass);
    $this->expectExceptionMessage($exceptionMessage);

    throw_unless($expression, $exception);
  }

  public function throwExceptionProvider()
  {
    return [
      [false, null, \Exception::class, 'Jawira\TheLostFunctions\throw_unless: expression is false'],
      [false, new \RuntimeException('Expression is false'), \RuntimeException::class, 'Expression is false'],
      [0, new \RuntimeException('Expression is 0'), \RuntimeException::class, 'Expression is 0'],
      ['', new \RuntimeException('Expression is empty string'), \RuntimeException::class, 'Expression is empty string'],
      ['0', new \RuntimeException('Expression is 0 string'), \RuntimeException::class, 'Expression is 0 string'],
      [null, new \RuntimeException('Expression is null'), \RuntimeException::class, 'Expression is null'],
      [[], new \RuntimeException('Expression is empty array'), \RuntimeException::class, 'Expression is empty array'],
    ];
  }
}
