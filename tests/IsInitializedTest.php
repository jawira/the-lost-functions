<?php

namespace Jawira\TheLostFunctions\Tests;

use PHPUnit\Framework\TestCase;
use function Jawira\TheLostFunctions\is_initialized;

class IsInitializedTest extends TestCase
{
  /**
   * @covers       \Jawira\TheLostFunctions\is_initialized
   * @dataProvider isInitializedProvider
   */
  public function testInitialized($propertyOne = 'age', $value = 10, $propertyTwo = 'age', $expected = true)
  {
    $dummy = new Dummy();
    $dummy->$propertyOne($value);
    $result = is_initialized($dummy, $propertyTwo);

    $this->assertIsBool($result);
    $this->assertSame($expected, $result);
  }

  public function isInitializedProvider()
  {
    return [
      ['name', 'junior', 'name', true],
      ['name', 'junior', 'age', false],
      ['name', 'junior', 'hasCar', false],
      ['age', 10, 'name', false],
      ['age', 10, 'age', true],
      ['age', 10, 'hasCar', false],
      ['hasCar', true, 'name', false],
      ['hasCar', true, 'age', false],
      ['hasCar', true, 'hasCar', true],
    ];
  }

  /**
   * @covers       \Jawira\TheLostFunctions\is_initialized
   */
  public function testNoPropertyException()
  {
    $this->expectException(\ReflectionException::class);
    $dummy = new Dummy();
    is_initialized($dummy, 'invalid');
  }
}
