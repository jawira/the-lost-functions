<?php

namespace Jawira\TheLostFunctions\Tests;

use PHPUnit\Framework\TestCase;
use SplFileObject;
use function Jawira\TheLostFunctions\temp_file;

class TempFileTest extends TestCase
{
  /**
   * @covers \Jawira\TheLostFunctions\temp_file
   */
  public function testFileTest()
  {
    $tempFile = temp_file('dummy');

    $this->assertInstanceOf(SplFileObject::class, $tempFile);
    $this->assertStringStartsWith('dummy', $tempFile->getFilename());
    $this->assertFileExists($tempFile->getPathname());
  }
}
