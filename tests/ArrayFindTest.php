<?php


namespace Jawira\TheLostFunctions\Tests;


use PHPUnit\Framework\TestCase;
use stdClass;
use function Jawira\TheLostFunctions\array_find;

class ArrayFindTest extends TestCase
{
  /**
   * @covers       \Jawira\TheLostFunctions\array_find
   * @dataProvider noCallbackProvider
   */
  public function testNoCallback($array, $expected)
  {
    $actual = array_find($array);

    $this->assertSame($expected, $actual);
  }

  public function noCallbackProvider()
  {
    return [
      [['', 'a', 'b', 'c'], 'a'],
      [[0, 1, 2, 3, 4, 5, 6], 1],
      [[false, null, 0, '', 'yes', 'maybe'], 'yes'],
    ];
  }

  /**
   * @covers       \Jawira\TheLostFunctions\array_find
   * @dataProvider callbackProvider
   */
  public function testCallback($array, $callback, $expected)
  {
    $actual = array_find($array, $callback);

    $this->assertSame($expected, $actual);
  }

  public function callbackProvider()
  {
    $searchLetter = function ($item) {
      return $item === 'b';
    };

    $name         = 'paco';
    $searchByName = function ($item) use ($name) {
      return $item->name === $name;
    };

    $user1       = new stdClass();
    $user1->name = 'hugo';
    $user2       = new stdClass();
    $user2->name = 'paco';
    $user3       = new stdClass();
    $user3->name = 'luis';

    return [
      [['a', 'b', 'c'], $searchLetter, 'b'],
      [[$user1, $user2, $user3,], $searchByName, $user2],
      [['abc', true, '2e2', '123'], 'is_numeric', '2e2'],
    ];
  }

  /**
   * @covers       \Jawira\TheLostFunctions\array_find
   * @dataProvider elementNotFoundProvider
   */
  public function testElementNotFound($array, $callback)
  {
    $actual = array_find($array, $callback);
    $this->assertNull($actual);
  }

  public function elementNotFoundProvider()
  {
    return [
      [['foo', 'bar', 'baz'], 'is_object'],
      [[false, 0, false, 0], null],
    ];
  }

  /**
   * @covers       \Jawira\TheLostFunctions\array_find
   * @dataProvider traversableProvider
   */
  public function testTraversable($array, $callback, $expected)
  {
    $actual = array_find($array, $callback);
    $this->assertSame($expected, $actual);
  }

  public function traversableProvider()
  {
    $x = new class() implements \Iterator {

      private int $position = 0;
      private array $elements = ['hana', 'dul', 'set', 'net'];

      public function current()
      {
        return $this->elements[$this->position];
      }

      public function next()
      {
        ++$this->position;
      }

      public function key()
      {
        return $this->position;
      }

      public function valid()
      {
        return array_key_exists($this->position, $this->elements);
      }

      public function rewind()
      {
        $this->position = 0;
      }
    };

    return [
      [$x, 'is_string', 'hana'],
      [$x, null, 'hana'],
      [$x, fn($e) => $e === 'set', 'set'],
      [$x, fn($e) => $e === 'net', 'net'],
      [$x, fn($e) => $e === 'blue', null],
    ];
  }
}
