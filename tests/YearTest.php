<?php

namespace Jawira\TheLostFunctions\Tests;

use DateTime;
use PHPUnit\Framework\TestCase;
use function Jawira\TheLostFunctions\year;

class YearTest extends TestCase
{
  /**
   * @covers \Jawira\TheLostFunctions\year
   */
  public function testSimpleCase()
  {
    $actual = year();
    $this->assertSame(date('Y'), $actual);
  }

  /**
   * @covers       \Jawira\TheLostFunctions\year
   * @dataProvider shortYearProvider
   */
  public function testShortYear($dateTime, $expected)
  {
    $actual = year(true, $dateTime);
    $this->assertIsString($actual);
    $this->assertSame($expected, $actual);

  }

  public function shortYearProvider()
  {
    return [
      [new DateTime(), date('y')],
      [new DateTime('1995-11-28'), '95'],
    ];
  }

  /**
   * @covers       \Jawira\TheLostFunctions\year
   * @dataProvider longYearProvider
   */
  public function testLongYear($dateTime, $expected)
  {
    $actual = year(false, $dateTime);
    $this->assertIsString($actual);
    $this->assertSame($expected, $actual);

  }

  public function longYearProvider()
  {
    return [
      [new DateTime(), date('Y')],
      [new DateTime('2020-11-28'), '2020'],
    ];
  }
}
