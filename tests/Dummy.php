<?php

namespace Jawira\TheLostFunctions\Tests;

/**
 * Used by {@see \Jawira\TheLostFunctions\Tests\IsInitializedTest}.
 */
class Dummy
{
  public string $name;
  private int $age;
  protected bool $hasCar;

  public function __call($method, $arguments)
  {
    $this->$method = reset($arguments);
  }
}
