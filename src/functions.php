<?php declare(strict_types=1);

namespace Jawira\TheLostFunctions;

use DateTime;
use DateTimeInterface;
use Exception;
use ReflectionClass;
use ReflectionException;
use SplFileObject;
use Throwable;
use function date;
use function intval;
use function is_null;
use function is_object;
use function is_string;
use function ltrim;
use function strrchr;
use function sys_get_temp_dir;
use function tempnam;

/**
 * @deprecated Use {@see \Jawira\TheLostFunctions\array_find} instead.
 */
function array_search_callback(array $array, ?callable $callback = null)
{
  return array_find($array, $callback);
}

/**
 * Find an array element using a callback.
 *
 * @param callable|null $callback
 * @return mixed
 * @link   https://jawira.gitlab.io/the-lost-functions/array_find.html
 * @author Jawira Portugal
 */
function array_find(iterable $array, ?callable $callback = null): mixed
{
  if (is_null($callback)) {
    $callback = fn($item) => !empty($item);
  }

  foreach ($array as $item) {
    if ($callback($item)) {
      return $item;
    }
  }

  return null;
}

/**
 * @deprecated Use {@see \Jawira\TheLostFunctions\get_short_class} instead.
 */
function get_class_short($object): string
{
  return get_short_class($object);
}

/**
 * Class name without the namespace
 *
 * @author Jawira Portugal
 * @link   https://jawira.gitlab.io/the-lost-functions/get_short_class.html
 */
function get_short_class(string|object $object): string
{
  if (is_object($object)) {
    $object = $object::class;
  }

  $shortClass = strrchr($object, '\\');

  if (false === $shortClass) {
    $shortClass = $object;
  }

  return ltrim($shortClass, '\\');
}

/**
 * Bytes in a string
 *
 * @author Jawira Portugal
 * @link https://jawira.gitlab.io/the-lost-functions/str_bytes.html
 */
function str_bytes(string $string): int
{
  return mb_strlen($string, '8bit');
}

/**
 * Thrown `$throwable` unless $expression is `true`.
 *
 * @throws Throwable
 * @link   https://jawira.gitlab.io/the-lost-functions/throw_unless.html
 * @author Jawira Portugal
 */
function throw_unless(mixed $expression, ?Throwable $throwable = null)
{
  if (!$throwable instanceof Throwable) {
    $throwable = new Exception(__FUNCTION__ . ': expression is false');
  }

  return $expression ?: throw $throwable;
}

/**
 * Current year.
 *
 * Year can be displayed with two or four digits.
 *
 * @param DateTimeInterface|null $dateTime
 * @return string
 * @link   https://jawira.gitlab.io/the-lost-functions/year.html
 * @author Jawira Portugal
 */
function year(bool $short = false, ?DateTimeInterface $dateTime = null): string
{
  $format = $short ? 'y' : 'Y';
  if (!$dateTime) {
    $dateTime = new DateTime('now');
  }

  return $dateTime->format($format);
}

/**
 * Range between current year and $startYar.
 *
 * @author Jawira Portugal
 * @link   https://jawira.gitlab.io/the-lost-functions/year_range.html
 */
function year_range(int $startYear): string
{
  /** @var string $currentYear */
  $currentYear = date('Y');
  $prepend     = $startYear < intval($currentYear) ? "{$startYear}\u{2013}" : '';

  return $prepend . $currentYear;
}

/**
 * Create temporary file using SplFileObject.
 *
 * @param string $prefix Prefix used in filename.
 * @return SplFileObject
 * @throws Exception
 * @author Jawira Portugal
 */
function temp_file(string $prefix): SplFileObject
{
  $tempFile = tempnam(sys_get_temp_dir(), $prefix);
  is_string($tempFile) ?: throw new Exception('Cannot create temporary file');
  return new SplFileObject($tempFile, 'w+');
}

/**
 * Tells if an object's property has been initialized.
 *
 * @param object $object Object to check.
 * @param string $property The name of the property to check.
 * @return bool True if provided property has been initialized.
 * @throws ReflectionException
 */
function is_initialized(object $object, string $property): bool
{
  $reflectionClass    = new ReflectionClass($object);
  $reflectionProperty = $reflectionClass->getProperty($property);
  $reflectionProperty->setAccessible(true); // @todo remove when php >= 8.1
  return $reflectionProperty->isInitialized($object);
}
