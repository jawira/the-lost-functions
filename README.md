The Lost Functions
==================

**Collection of convenience functions for PHP.**

[![pipeline status](https://gitlab.com/jawira/the-lost-functions/badges/master/pipeline.svg)](https://gitlab.com/jawira/the-lost-functions/-/commits/master)
[![coverage report](https://gitlab.com/jawira/the-lost-functions/badges/master/coverage.svg)](https://gitlab.com/jawira/the-lost-functions/-/commits/master)

Functions
---------

- [array_find()](https://jawira.gitlab.io/the-lost-functions/array_find.html) - Search an element
  using a callback.
- [get_short_class()](https://jawira.gitlab.io/the-lost-functions/get_short_class.html) - Get the short name of an
  object.
- [str_bytes()](https://jawira.gitlab.io/the-lost-functions/str_bytes.html) - Get string length in bytes.
- [temp_file()](https://jawira.gitlab.io/the-lost-functions/temp_file.html): - Create temporary file.
- [throw_unless()](https://jawira.gitlab.io/the-lost-functions/throw_unless.html) - Throws an exception if an assertion
  is false.
- [year()](https://jawira.gitlab.io/the-lost-functions/year.html) - Returns current year.
- [year_range()](https://jawira.gitlab.io/the-lost-functions/year_range.html) - Range between start year and current
  year.

How to install
--------------

```console
composer require jawira/the-lost-functions
```

Contributing
------------

If you liked this project,
⭐ [star it on GitLab](https://gitlab.com/jawira/the-lost-functions).

License
-------

This library is licensed under the [MIT license](LICENSE.md).
