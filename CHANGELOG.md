## v1.4.3 (2021-08-29)

### changed (1 change)

- [Improve documentation menu](jawira/the-lost-functions@0c9d8a357d252525332632b542ba1bcc545739a8) ([merge request](jawira/the-lost-functions!19))

### removed (1 change)

- [Remove CHANGELOG.md](jawira/the-lost-functions@401419f4582690b9d3a286d0c3191bd454c6a0bb) ([merge request](jawira/the-lost-functions!19))

### added (1 change)

- [Create changelog with CI](jawira/the-lost-functions@28237947492ca58508ebff9d6e7f17af6ca6ac2a) ([merge request](jawira/the-lost-functions!17))
